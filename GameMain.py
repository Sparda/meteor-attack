import pygame
import os
import random

pygame.init()
width, height = 600, 800
display = pygame.display
win = display.set_mode((width, height))
display.set_caption("Meteor Attack")
clock = pygame.time.Clock()

limit_border = pygame.Rect(0, 600, width, 10)

menu_music = pygame.mixer.Sound(os.path.join('Meteor Attack\Assets', 'menu_music.mp3'))
bg_music = pygame.mixer.Sound(os.path.join('Meteor Attack\Assets', 'bg_music.mp3'))
score_music = pygame.mixer.Sound(os.path.join('Meteor Attack\Assets', 'score_music.mp3'))
char_sound = pygame.mixer.Sound(os.path.join('Meteor Attack\Assets', 'char_insert_sound.mp3'))
button_accept_sound = pygame.mixer.Sound(os.path.join('Meteor Attack\Assets', 'Button_accept.mp3'))
button_decline_sound = pygame.mixer.Sound(os.path.join('Meteor Attack\Assets', 'Button_decline.mp3'))
bullet_hit_sound = pygame.mixer.Sound(os.path.join('Meteor Attack\Assets', 'Meteor destroyed.mp3'))
bullet_fire_sound = pygame.mixer.Sound(os.path.join('Meteor Attack\Assets', 'Laser sound effect.mp3'))
char_sound.set_volume(0.2)
button_accept_sound.set_volume(0.2)
button_decline_sound.set_volume(0.2)
bullet_hit_sound.set_volume(0.2)
bullet_fire_sound.set_volume(0.2)
bg_music.set_volume(0.1)
score_music.set_volume(0.1)
menu_music.set_volume(0.1)

life = 100
lifeScale = 2
lifeBar = pygame.Rect ((380, 750, life * lifeScale, 20))
damageBar = pygame.Rect ((380, 750, 0, 20))
score = 0
fps = 55
velocity = 6
bullet_vel = 10
asteroid_velocity = 3.0
players = []

start_button_image = pygame.image.load(os.path.join('Meteor Attack\Assets', 'start_button.png'))
start_button = pygame.transform.scale(start_button_image, (200, 100))
start_button_pressed_image = pygame.image.load(os.path.join('Meteor Attack\Assets', 'start_button_pressed.png'))
start_button_pressed = pygame.transform.scale(start_button_pressed_image, (200, 100))
spaceship_image = pygame.image.load(os.path.join('Meteor Attack\Assets', 'PurpleSpaceShip.png'))
spaceship = pygame.transform.scale(spaceship_image, (50, 50))
asteroids_image = pygame.image.load(os.path.join('Meteor Attack\Assets', 'asteroids.png'))
universe_image = pygame.image.load(os.path.join('Meteor Attack\Assets', 'space.jpg'))
universe = pygame.transform.scale(universe_image, (width, height))
smashedAsteroids_image = pygame.image.load(os.path.join('Meteor Attack\Assets', 'asteroids_smashed.png'))

class Player:
    def __init__(self, id, name, score, wave):
        self.id = id
        self.name = name
        self.score = score
        self.wave = wave

class Button:
    def __init__(self, x, y, image):
        self.image = image
        self.rect = self.image.get_rect()
        self.rect.topleft = (x, y)

class Asteroid:
    def __init__(self, obj, id):
        self.obj = obj
        self.id = id

class DestroyedAsteroid:
    def __init__(self, cood, id):
        self.cood = cood
        self.id = id

def get_image(sheet, frame, width, height):
    image = pygame.Surface((width, height), pygame.SRCALPHA, 32).convert_alpha()
    image.blit(sheet, (0, 0), ((frame * width), 0, width, height))

    return image

def draw_text(window, text, size, x, y, color):
    draw_sys = pygame.font.SysFont("Arial", size)
    string = text
    formated_text = draw_sys.render(string, 1, pygame.color.Color(color))
    window.blit(formated_text, (x, y))

asteroid_sprite1 = pygame.transform.scale(get_image(asteroids_image, 0, 64, 64), (40, 40))
asteroid_sprite2 = pygame.transform.scale(get_image(asteroids_image, 1, 64,64), (40, 40))
asteroid_sprite3 = pygame.transform.scale(get_image(asteroids_image, 2, 64, 64), (40, 40))
destroyedAsteroid_sprite1 = pygame.transform.scale(get_image(smashedAsteroids_image, 0, 64, 64), (40, 40))
destroyedAsteroid_sprite2 = pygame.transform.scale(get_image(smashedAsteroids_image, 1, 64, 64), (40, 40))
destroyedAsteroid_sprite3 = pygame.transform.scale(get_image(smashedAsteroids_image, 2, 64, 64), (40, 40))

def draw_window(player, asteroidList, bulletList, destroyedList, score, wave):
    win.blit(universe, (0, 0))
    draw_text(win, 'Score: ' + str(score), 50, 80, 700, 'white' )
    draw_text(win, 'Wave: ' + str(wave), 50, 80, 650, 'white')
    pygame.draw.rect(win, pygame.color.Color('dark red'), limit_border)
    pygame.draw.rect(win, pygame.color.Color('red'), lifeBar)
    pygame.draw.rect(win, pygame.color.Color('yellow'), damageBar)
    if asteroidList == []:
        pass
    else:
        for asteroid in asteroidList:
            if asteroid.obj == None:
                pass
            else:
                if asteroid.id == 1:
                    win.blit(asteroid_sprite1, (asteroid.obj.x, asteroid.obj.y))
                elif asteroid.id == 2:
                    win.blit(asteroid_sprite2, (asteroid.obj.x, asteroid.obj.y))
                elif asteroid.id == 3:
                    win.blit(asteroid_sprite3, (asteroid.obj.x, asteroid.obj.y))
    if destroyedList == []:
        pass
    else:
        for asteroid in destroyedList:
            if asteroid.id == 1:
                win.blit(destroyedAsteroid_sprite1, (asteroid.cood[0], asteroid.cood[1]))
            elif asteroid.id == 2:
                win.blit(destroyedAsteroid_sprite2, (asteroid.cood[0], asteroid.cood[1]))
            elif asteroid.id == 3:
                win.blit(destroyedAsteroid_sprite3, (asteroid.cood[0], asteroid.cood[1]))
    win.blit(spaceship, (player.x, player.y))
    if bulletList == []:
        pass
    else:
        for bullet in bulletList:
            pygame.draw.rect(win, pygame.color.Color('red'), bullet)
    display.update()

def save_cood_asteroid(asteroid):
    asteroid_x = asteroid.obj.x
    asteroid_y = asteroid.obj. y
    return asteroid_x, asteroid_y

def player_handle_event(player, keys_pressed, vel_mod):
    if keys_pressed[pygame.K_a] and player.x - velocity > 0:
        player.x -= velocity + vel_mod
    if keys_pressed[pygame.K_d] and player.x + velocity + player.width < 600:
        player.x += velocity + vel_mod
    if keys_pressed[pygame.K_w] and player.y - velocity > 600:
        player.y -= velocity + vel_mod
    if keys_pressed[pygame.K_s] and player.y + velocity + player.height < 800:
        player.y += velocity + vel_mod

def bullet_handle_event(bullets, asteroidList, destroyedList):
    for bullet in bullets:
        bullet.y -= bullet_vel
        for asteroid in asteroidList:
            if asteroid.obj == None:
                pass
            else:
                if asteroid.obj.colliderect(bullet) and bullet in bullets:
                    bullets.remove(bullet)
                    destroyedAsteroid = DestroyedAsteroid(save_cood_asteroid(asteroid), asteroid.id)
                    asteroidList.remove(asteroid)
                    asteroid.obj = None
                    asteroidList.append(asteroid)
                    destroyedList.append(destroyedAsteroid)
                    bullet_hit_sound.play()
        if bullet == None:
            pass
        else:
            if bullet.y - bullet_vel < 0 and bullet in bullets:
                bullets.remove(bullet)

def is_player_dead():
    if damageBar.width >= lifeBar.width:
        return True
    else:
        return False

def asteroidSpawn(asteroidList):
    asteroid = Asteroid(pygame.Rect(random.randint(0, 600 - 40), 0, 40, 40), random.randint(1,3))
    asteroidList.append(asteroid)
    
def asteroid_handle_movement(asteroidList, vel_mod):
    for asteroid in asteroidList:
        if asteroid.obj == None:
            pass
        else:
            asteroid.obj.y += asteroid_velocity + vel_mod
            if asteroid.obj.y + asteroid.obj.height >= limit_border.y + limit_border.height:
                asteroidList.remove(asteroid)
                asteroid.obj = None
                asteroidList.append(asteroid) 
                damageBar.width += 10 * lifeScale

def menu_screen():
    menu_music.play(-1)
    win.blit(universe, (0, 0))
    draw_text(win, "Meteor Attack", 64,  width/2 - 150, height/4, 'red')
    draw_text(win, "w, a, s, d to Move, Space to fire", 22, width/2 - 130, height/2, 'white')
    draw_text(win, "Press \'Space\' to begin", 18, width/2 - 62, height*3/4, 'white')
    display.flip()
    waiting = True
    while waiting:
        clock.tick(fps)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_SPACE:
                    button_accept_sound.play()
                    waiting = False
                
def score_screen(record):
    score_music.play(-1)
    win.blit(universe, (0, 0))
    draw_text(win, "Score Table ", 64,  width/2 - 150, height/4, 'red')
    draw_text(win, "User: " + players[0].name, 22,  width/2 - 150, height/4 + 100, 'yellow')
    draw_text(win, "Score: " + str(record[0]), 22, width/2 - 150, height/2, 'yellow')
    draw_text(win, "Waves complete: " + str(record[1]), 22, width/2 - 150, height/2 + 100, 'yellow')
    draw_text(win, "Press \'ESC\' to back to menu screen", 18, width/2 - 135, height/2 + 200, 'white')
    display.flip()
    waiting = True
    while waiting:
        clock.tick(fps)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_ESCAPE:
                    button_accept_sound.play()
                    "Save player data on database"   
                    waiting = False

def user_screen():
    submit_button = Button(width/2 - 100, height/2 + 30, start_button)
    draw_sys = pygame.font.SysFont('comic sans mc', 50)
    text = ''
    text_id = ''
    text_limit = 25
    mouse_pressed = False
    mouse_label = False
    button_image = start_button
    color_passive = pygame.color.Color('dark grey')
    color_active = pygame.color.Color('black')
    color = color_passive
    colorText = pygame.color.Color('black')

    active = False

    rect = pygame.Rect(width/2 - 250, height/2 - 105, 485, 250)
    input_rect = pygame.Rect(width/2 - 242, height/2 - 67, 469, 64)
    bg_input_rect = pygame.Rect(width/2 - 240, height/2 - 65, 465, 60)

    waiting = True
    while waiting:
        mouse_pos = pygame.mouse.get_pos()
        if input_rect.collidepoint(mouse_pos):
            if pygame.mouse.get_pressed()[0]:
                mouse_label = True
            else:
                if mouse_label:
                    active = True
                    text = ''
                    colorText = pygame.color.Color('black')
                    color = color_active
                    mouse_label = False
        else:
            if pygame.mouse.get_pressed()[0]:
                mouse_label = True
            else:
                if mouse_label:
                    active = False
                    color = color_passive
                    mouse_label = False

        if submit_button.rect.collidepoint(mouse_pos):
            if pygame.mouse.get_pressed()[0]:
                mouse_pressed = True
                button_image = start_button_pressed
            else:
                if mouse_pressed:
                    if text != '':
                        if text == 'Type something pls':
                            button_decline_sound.play()
                        else:
                            players.append(Player(text_id, text, 0, 0))
                            button_accept_sound.play()
                            waiting = False
                    
                    else:
                        text = 'Type something pls'
                        colorText = pygame.color.Color('dark grey')
                        active = False
                        color = color_passive
                        button_decline_sound.play()
                    button_image = start_button
                    mouse_pressed = False
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()

            if event.type == pygame.KEYDOWN:
                if active:
                    if event.key == pygame.K_BACKSPACE:
                        text = text [:-1]
                    elif len("USERNAME: " + text) < text_limit:
                        oldlen = len(text)
                        text += event.unicode
                        if oldlen < len(text):
                            char_sound.play()
                        

        win.blit(universe, (0, 0))
        pygame.draw.rect(win, pygame.color.Color('dark grey'), rect)
        pygame.draw.rect(win, pygame.color.Color('white'), bg_input_rect)
        pygame.draw.rect(win, color, input_rect, 2)
        username_text = draw_sys.render("USERNAME:", 1, pygame.color.Color('grey'))
        formated_text = draw_sys.render(text, 1, colorText)
        win.blit(button_image, (submit_button.rect.x, submit_button.rect.y))
        win.blit(username_text, (width/2 - 245, height/2 - 100))
        win.blit(formated_text, (width/2 - 235, height/2 - 50))

        display.flip()
        clock.tick(30)

def main():
    run = True
    game_menu = True
    while run:
        if game_menu:
            menu_screen()
            menu_music.stop()
            bg_music.play(-1)
            game_menu = False
            player = pygame.Rect(600/2 - 25, 800 - 50, 50, 50)
            bullets = []
            asteroids = []
            destroyedAsteroids = []
            asteroid_count = 0
            last_count = 0
            max_bullets = 5
            max_asteroid = 20
            vel_mod = 0
            wave = 1
            score = 0
            lastScore = 0
            record = 0

        clock.tick(fps)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE and len(bullets) < max_bullets:
                    bullet = pygame.Rect(player.x + player.width//2 - 2, player.y, 5, 15)
                    bullets.append(bullet)
                    bullet_fire_sound.play()
        score = len(destroyedAsteroids) + lastScore
        keys_pressed = pygame.key.get_pressed()
        if len(destroyedAsteroids) < max_asteroid:
            if asteroids == []:
                asteroidSpawn(asteroids)
                last_count = asteroid_count
                asteroid_count += 1
            elif asteroids != []:
                if asteroids[last_count].obj == None:
                    asteroidSpawn(asteroids)
                    last_count = asteroid_count
                    asteroid_count += 1
                else:
                    if asteroids[last_count].obj.y > 300:
                        asteroidSpawn(asteroids)
                        last_count = asteroid_count
                        asteroid_count += 1
        else:
            asteroids = []
            asteroid_count = 0
            wave += 1
            vel_mod += 0.3
            lastScore += max_asteroid
            max_asteroid += 5               
            destroyedAsteroids = []
        player_handle_event(player, keys_pressed, vel_mod)
        asteroid_handle_movement(asteroids, vel_mod)
        bullet_handle_event(bullets, asteroids, destroyedAsteroids) 
        draw_window(player, asteroids, bullets, destroyedAsteroids, score, wave)

        if is_player_dead():
            game_menu = True
            bg_music.stop()
            players[0].score = score
            players[0].wave = wave - 1
            record = (score, wave)
            score_screen(record)
            
            score_music.stop()
            damageBar.width = 0

    print('Game Finished')
    pygame.quit()

if __name__ == '__main__':
    user_screen()
    main()