import mysql.connector
from mysql.connector import Error
from GameMain import score as s

create_table_users = """CREATE TABLE tbl_users (Id int PRIMARY KEY AUTO_INCREMENT, Name VARCHAR(70) NOT NULL, Age int(11) NOT NULL)"""
insert_data_users = """INSERT INTO tbl_users (Id, Name, Age) VALUES """
create_table_score = """CREATE TABLE tbl_score (UserID int PRIMARY KEY, FOREIGN KEY(UserID) REFERENCES tbl_users(Id), Username VARCHAR(70) NOT NULL, Score int(11) NOT NULL, Waves int(11) NOT NULL)"""
insert_data_score = """INSERT INTO tbl_score (UserID, Username, Score, Waves) VALUES """

def connect_to_database():
    try:
        global con
        con = mysql.connector.connect (host = 'localhost', database = 'meteorattackdb', user = 'root', password = 'bablubabalu')
    except Error as e:
        print('Connection problem: ', e)

def showAll(tbl_type):
    try:
        connect_to_database()
        selectTable = """SELECT * FROM """ + tbl_type
        cursor = con.cursor()
        cursor.execute(selectTable)
        for i in cursor:
            print(i)
    except Error as e:
        print('Failed to get table: ', e)
    finally:
        if con.is_connected():
            cursor.close()
            con.close()

def check(tbl_type, ID):
    try:
        connect_to_database()
        if tbl_type == 'tbl_users':
            check_sql = "select * from " + tbl_type + " WHERE ID = " + ID
            cursor = con.cursor()
            cursor.execute(check_sql)
            lines = cursor.fetchall()

            for line in lines:
                print("ID:", line[0], "\nName:", line[1], "\nAge:", line[2])
        
        elif tbl_type == 'tbl_score':
            check_sql = "select * from " + tbl_type + " WHERE UserID = " + ID
            cursor = con.cursor()
            cursor.execute(check_sql)
            lines = cursor.fetchall()

            for line in lines:
                print("UserID:", line[0], "\nUsername:", line[1], "\nScore:", line[2], "\nWaves:", line[3])
    except Error as e:
        print('Fail to check: ' + e)
    finally:
        if con.is_connected():
            cursor.close()
            con.close()

def create_tb(create_table):
    try:
        connect_to_database()
        cursor = con.cursor()
        cursor.execute(create_table)
        print('Table created successfully')
    except Error as e:
        print('Failed to create table: ', e)
    finally:
        if con.is_connected():
            cursor.close()
            con.close()

def insert_dt():
    try:
        connect_to_database()
        print('Initializing data insertion...')
        table = input('Which table you wanna modify? \'1\' for tbl_users, \'2\' for tbl_score\n')
        quantity = input('Type how many datas you want to insert: ')
        if quantity == '0':
            pass
        else:
            if table == '1':
                insert_data = insert_data_users
                print('Inserting users data...')
                for i in range(int(quantity)):    
                    Id = input("User Id: ")
                    name = input("Username: ")
                    age = input('Age: ')

                    data = '(' + Id + ', \'' + name + '\', ' + age + ')'
                    if i == int(quantity) - 1:
                        insert_data += data
                    else:
                        insert_data += data + ', '

            elif table == '2':
                insert_data = insert_data_score
                print('Inserting score data...')
                for i in range(int(quantity)):    
                    userID = input("User ID: ")
                    username = input("Username: ")
                    score = s
                    wave = input("Waves completed: ")

                    data = '(' + userID + ',\'' + username + '\',' + score + ',' + wave + ')'
                    if i == int(quantity) - 1:
                        insert_data += data
                    else:
                        insert_data += data + ', '

            cursor = con.cursor()
            cursor.execute(insert_data)
            con.commit()
            print(cursor.rowcount, 'data(s) inserted successfully')
            if table == '1':
                showAll('tbl_users')
            elif table == '2':
                showAll('tbl_score')

    except Error as e:
        print('Failed to insert data(s): ', e)
    finally:
        if con.is_connected():
            if quantity != '0':
                cursor.close()
            con.close()

def update():
    try:
        connect_to_database()
        print('Initializing data update... ')
        tableType = input ('Which table you wanna update? \'1\' for tbl_users, \'2\' for tbl_score\n')
        if tableType == '1':
            showAll('tbl_users')
            connect_to_database()
            Id = input('Type User ID: ')
            check('tbl_users', Id)
            connect_to_database()
            dataType = input('Type the data type you want to change (\'n\' for Name or \'a\' for Age): ')
            if dataType == 'n':
                print('Update user name in database')
                print('\nInsert new user name:')
                name = input('New name: ')
                declaration = """UPDATE tbl_users    
                SET Name = \'""" + name + """\' WHERE Id = """ + Id
            elif dataType == 'a':
                print('Update age in database')
                print('\nInsert new user age: ')
                age = input('New age: ')
                declaration = """UPDATE tbl_users    
                SET Age = """ + age + """ WHERE Id = """ + Id
        elif tableType == '2':
            showAll('tbl_score')
            connect_to_database()
            userID = input('Type UserID: ')
            check('tbl_score', userID)
            connect_to_database()
            dataType = input('Type the data type you want to change (\'s\' for Score or \'w\' for Waves Completed): ')
            if dataType == 's':
                print('Update score in database')
                print('\nInsert new user score: ')
                score = input('New score: ')
                declaration = """UPDATE tbl_score    
                SET Score = """ + score + """ WHERE UserID = """ + userID
            elif dataType == 'w':
                print('Update waves quantity in database')
                print('\nInsert new waves quantity: ')
                waves = input('New quantity: ')
                declaration = """UPDATE tbl_score    
                SET Waves = """ + waves + """ WHERE UserID = """ + userID
        cursor = con.cursor()
        cursor.execute(declaration)
        con.commit()
        print("Data updated successfully")
        verify = input("\nWish check the update? y = yes, n = no: ")
        if verify == 'y' and tableType == '1':
            check('tbl_users', Id)
        elif verify == 'y' and tableType == '2':
            check('tbl_score', userID)
    except Error as e:
        print('Failed to update:', e)
    finally:
        if con.is_connected():
            cursor.close()
            con.close()

def delete():
    try:
        connect_to_database()
        print('Initializing data deletion...')
        tableType = input('Which table you wanna modify? \'1\' for tbl_users, \'2\' for tbl_score\n')
        if tableType == '1':
            showAll('tbl_users')
            connect_to_database()
            Id = input('Type the ID from the user which you wanna delete: ')
            delfunc = """DELETE FROM tbl_users WHERE ID = """ + Id
        elif tableType == '2':
            showAll('tbl_score')
            connect_to_database()
            userID = input('Type the ID from the score data which you wanna delete: ')
            delfunc = """DELETE FROM tbl_score WHERE UserID = """ + userID
        cursor = con.cursor()
        cursor.execute(delfunc)
        con.commit()
        print('Data removed succesfully')
        verify = input("\nWish check the update? y = yes, n = no: ")
        if verify == 'y' and tableType == '1':
            showAll('tbl_users')
        elif verify == 'y' and tableType == '2':
            showAll('tbl_score')
    except Error as e:
        print('Failed to delete data: ', e)
    finally:
        if con.is_connected():
            cursor.close()
            con.close()

if __name__ == '__main__':
    showAll('tbl_users')
    showAll('tbl_score')
    insert_dt()
    update()
    delete()

    print("Goodbye!")